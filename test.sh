#!/bin/bash
set -x

TMP_ROLE_PATH=$(mktemp -d -t ci-XXXXXXXXXX)/roles

mkdir -p $TMP_ROLE_PATH

#ansible-galaxy install --force -r molecule/default/requirements.yml -p $TMP_ROLE_PATH
ln -s $(pwd) $TMP_ROLE_PATH/remove-apparmor

export ANSIBLE_ROLES_PATH=$ANSIBLE_ROLES_PATH:$TMP_ROLE_PATH

# warning the , is mandatory...
ansible-playbook -vvv -i 192.168.105.165, -u root molecule/default/playbook.yml
